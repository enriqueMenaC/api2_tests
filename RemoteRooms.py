# PRODUCTION: RookMotion SUM
roomsBranch1 = []
roomsBranch1.append({"branch_id": 1, "room_coach": "Javier",
                     "room_uuid": "01532cb0-dcd2-411c-91bb-ec6f86e40548",
                     "token_level": "branch_rmy9ein3ekk9vgjdyo5xcvxfisymz"})
roomsBranch1.append({"branch_id": 1, "room_coach": "Root",
                     "room_uuid": "049a0657-0e5f-42ef-b002-453f4ea0126b",
                     "token_level": "branch_rmy9ein3ekk9vgjdyo5xcvxfisymz"})
roomsBranch1.append({"branch_id": 1, "room_coach": "Enrique",
                     "room_uuid": "b30df87a-1f7d-477a-a688-5cebd8c64c71",
                     "token_level": "branch_rmy9ein3ekk9vgjdyo5xcvxfisymz"})

# PRODUCTION: SALES
roomsBranch2 = []
roomsBranch2.append({"branch_id": 94, "room_coach": "Cardio",
                     "room_uuid": "f38c47a8-f75f-4667-bdcc-de8b2e2c1dfc",
                     "token_level": "branch_d4jqrqqpdzluf6uib7e9lyv2grwp9"})
roomsBranch2.append({"branch_id": 94, "room_coach": "Full",
                     "room_uuid": "b3990ba4-e74c-4323-b73b-b2d4ef5a3d0b",
                     "token_level": "branch_d4jqrqqpdzluf6uib7e9lyv2grwp9"})
roomsBranch2.append({"branch_id": 94, "room_coach": "Jesus",
                     "room_uuid": "223934e9-3019-4e33-8b57-1b17e31a40c6",
                     "token_level": "branch_d4jqrqqpdzluf6uib7e9lyv2grwp9"})
roomsBranch2.append({"branch_id": 94, "room_coach": "Kick",
                     "room_uuid": "c8f9d834-d212-4b45-9ca2-2629f8f3297e",
                     "token_level": "branch_d4jqrqqpdzluf6uib7e9lyv2grwp9"})
roomsBranch2.append({"branch_id": 94, "room_coach": "Prueba",
                     "room_uuid": "1b5a9387-b950-476d-b912-194ae6642295",
                     "token_level": "branch_d4jqrqqpdzluf6uib7e9lyv2grwp9"})
roomsBranch2.append({"branch_id": 94, "room_coach": "Pao",
                     "room_uuid": "d1c1e9b1-fad0-4607-a344-49b25e266434",
                     "token_level": "branch_d4jqrqqpdzluf6uib7e9lyv2grwp9"})

# DEVELOP: RookMotion SUM
roomsBranch3 = []
roomsBranch3.append({"branch_id": 1, "room_coach": "Tom",
                     "room_uuid": "4a100e60-3f0d-4e4f-94da-35a9a9204104",
                     "token_level": "branch_wg4NJ1yIDW5nZJ8m8TlOZvojBkXH2"})
roomsBranch3.append({"branch_id": 1, "room_coach": "Javier",
                    "room_uuid": "767652b0-9a5d-41cc-8303-d34496329fbe",
                    "token_level": "branch_wg4NJ1yIDW5nZJ8m8TlOZvojBkXH2"})
roomsBranch3.append({"branch_id": 4, "room_coach": "Tom",
                     "room_uuid": "49bc46dc-82a2-4c02-b897-439687b7c279",
                     "token_level": "client_hotSVhXiKcpzhRvtb3h1DJMYTZ6SA"})

# SANDBOX: Gerardo
roomsBranch4 = []
roomsBranch4.append({"branch_id": 186, "room_coach": "Daenerys",
                     "room_uuid": "6e69627b-98d7-4ce6-af98-366aeea8e5d1",
                     "token_level": "branch_UrBWEghaM1gyQKaBKz1zIJuM3BMRK"})
roomsBranch4.append({"branch_id": 186, "room_coach": "Tyrion",
                     "room_uuid": "7fa34993-ebdb-4d0a-8d11-fe7b41f338d1",
                     "token_level": "branch_UrBWEghaM1gyQKaBKz1zIJuM3BMRK"})
roomsBranch4.append({"branch_id": 186, "room_coach": "Arya",
                     "room_uuid": "7e815411-7a3b-448b-8e71-d94e0ac6cce4",
                     "token_level": "branch_UrBWEghaM1gyQKaBKz1zIJuM3BMRK"})
