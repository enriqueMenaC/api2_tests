import __env__

class_config = {}
class_config["training_type_uuid"] = __env__.training_type_uuid
class_config["duration"] = 60
class_config["class_delay"] = 0
class_config["capacity"] = 25
class_config["user_capacity_status"] = 1
class_config["data_order"] = "calories"
