import inspect
import json
import os
import sys

import __env__
import requests


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from rookmotion_utilities.DebugHelper import get_this_method_name
from rookmotion_utilities.LoggerHelper import Logger
from rookmotion_api2.APICommon import headers, response_is_ok


logger = Logger(name=os.path.basename(__file__), level="debug")


def login_admin(token_level_, email_, password_):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/auth/admin/login"
        headers_ = headers.copy()
        headers_["token-level"] = token_level_

        payload = {}
        payload["email"] = email_
        payload["password"] = password_

        payload = json.dumps(payload)
        response = requests.post(url, headers=headers_, data=payload, timeout=10)
        if response_is_ok(response):
            logger.info("admin_email: {} logged. {}".format(email_, response.json()))
            return response.json()
        else:
            logger.error("Failed to login admin_email: {}. {}".format(email_, response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, email_), e)


if __name__ == "__main__":
    token_level = "branch_UrBWEghaM1gyQKaBKz1zIJuM3BMRK"
    email = "root@rookmotion.com"
    password = "#1PDlqJ2$3xX"
    login_admin(token_level, email, password)
