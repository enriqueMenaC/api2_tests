import datetime
import inspect
import json
import os
import sys
import time
import random
from typing import Dict

import __env__
import requests


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from rookmotion_utilities.DebugHelper import get_this_method_name
from rookmotion_utilities.LoggerHelper import Logger
from rookmotion_api2.APICommon import headers, response_is_ok, string_to_dictionary, get_paginated_items


logger = Logger(name=os.path.basename(__file__), level="debug")


def get_sensors_from_center(from_date: str = None):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/center/sensors"
        if from_date is not None:
            url += "?from_date={}".format(from_date)

        logger.info("Getting sensors from center since {}".format(from_date))
        sensors = get_paginated_items("center/sensors", url, headers)
        return sensors
    except Exception as e:
        logger.exception("{}: ".format(function_name), e)


def get_sensors_from_user(user_uuid_):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "/api/v2/sensors/user/{}".format(user_uuid_)
        sensors = get_paginated_items('sensors/user/{}'.format(user_uuid_), url, headers)
        return sensors
    except Exception as e:
        logger.exception("{}: {}".format(function_name, user_uuid_), e)


def add_sensor_to_user(sensor_uuid_, user_uuid_):
    function_name = get_this_method_name()
    try:
        logger.test("En obra negra")
    except Exception as e:
        logger.exception("{}: user {} sensor {}".format(function_name, user_uuid_, sensor_uuid_), e)



