import inspect
import json
import os
import sys

import __env__
import requests

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from rookmotion_utilities.LoggerHelper import Logger


logger = Logger(name='APICommon', level="debug")

domain = __env__.api_hostname
token_level = __env__.token_level
token_auth = __env__.bearer

headers = {
    'token-level': token_level,
    'Accept-Language': 'en',
    'Accept': 'application/json',
    'Authorization': token_auth,
    'Content-Type': 'application/json',

    'key-device': "12345678",
    'product': "PC",
    'model': "T470s",
    'version': "1.0"
}


def string_to_dictionary(serialized):
    try:
        data_json = serialized.text
        data_json = json.loads(data_json)
        return data_json
    except Exception as e:
        logger.exception("string_to_dictionary", e)
    return None


def print_elapsed_time(action: str, response: requests.Response):
    try:
        elapsed_time = response.elapsed.total_seconds()
        logger.server_info("Request: {} took: {:.2f} s".format(action, elapsed_time))
    except Exception as e:
        logger.exception("print_elapsed_time", e)
    return None


def response_is_ok(response: requests.Response):
    try:
        status_code = response.status_code
        if status_code == 200 or status_code == 201:
            return True
        elif status_code == 401 or status_code == 404:
            logger.error("Wrong server credentials")
        elif status_code == 409:
            logger.error("Conflict at request")
        else:
            logger.error("Response not ok: {} url {}".format(response, response.url))
        logger.test(response.text)
        return False
    except Exception as e:
        logger.exception("response_is_ok", e)


def server_is_alive():
    return get_server_time()


def get_server_time():
    api_action = "api/v2/server/datetime"
    url = domain + api_action
    item_type = 'Server datetime'

    try:
        response = requests.get(url, headers=headers, timeout=10)
        print_elapsed_time(api_action, response)
        if response_is_ok(response):
            response = string_to_dictionary(response)
            server_datetime = response.get('datetime', None)
            if server_datetime is not None:
                logger.info("Server datetime: {}".format(server_datetime))
                return True

    except Exception as e:
        if type(e).__name__ == "ConnectionError":
            logger.error("Exception getting {}: Internet not available".format(item_type))
        elif type(e).__name__ == "SSLError":  # This happened on a network which required a web login
            logger.error("Exception getting {}: Internet not available".format(item_type))
        else:
            logger.exception("Getting center info", e)
    return None


def get_paginated_items(item_type: str, url: str, headers_: str):
    try:
        paginating = True
        items = []
        page = 1

        while paginating:
            try:
                response = requests.get(url, headers=headers_, timeout=10)
                print_elapsed_time("/{}?page={}".format(item_type, page), response)
                if response_is_ok(response):
                    response = string_to_dictionary(response)
                    data = response["data"]
                    links = response["links"]
                    if data is not None:
                        items += data

                    if links["next"] is not None:
                        url = links["next"]
                        page += 1
                    else:
                        paginating = False
                else:
                    paginating = False

            except Exception as e:
                paginating = False
                if type(e).__name__ == "ConnectionError":
                    logger.error("Exception getting {}: Internet not available".format(item_type))
                else:
                    logger.exception("Getting {} pagination".format(item_type), e)
        return items
    except Exception as e:
        logger.exception("Getting {} pagination".format(item_type), e)

    return None


if __name__ == "__main__":
    server_is_alive()
