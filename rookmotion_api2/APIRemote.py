import inspect
import json
import os
import sys

import __env__
import requests


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from rookmotion_utilities.DebugHelper import get_this_method_name
from rookmotion_utilities.LoggerHelper import Logger
from rookmotion_api2.APICommon import headers


logger = Logger(name=os.path.basename(__file__), level="debug")


def request_room_access(user_uuid_, token_user_, class_uuid_):
    function_name = get_this_method_name()
    try:

        url = __env__.api_hostname + "api/v2/room/configuration/{}".format(class_uuid_)
        headers_ = headers.copy()
        headers_["token-user"] = token_user_

        payload = {}
        payload["user_uuid"] = user_uuid_

        payload = json.dumps(payload)
        response = requests.post(url, headers=headers_, data=payload, timeout=10)

        if response.status_code == 307 or response.status_code == 201:
            logger.info("user_uuid: {} accepted into class: {}. {}".format(user_uuid_, class_uuid_, response.json()))
            return response.json()
        else:
            logger.error("{} user_uuid: {}: class_uuid: {}. Response: {}".
                         format(function_name, user_uuid_, class_uuid_, response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, user_uuid_), e)


def create_class(room_uuid_, class_config, token_admin, token_level):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/room/{}/configuration".format(room_uuid_)
        headers_ = headers.copy()
        headers_["token-admin"] = token_admin
        headers_["token-level"] = token_level
        print("Using: {}".format(token_admin))
        print("Using: {}".format(token_level))
        payload = class_config
        payload = json.dumps(payload)
        response = requests.post(url, headers=headers_, data=payload, timeout=10)
        if response.status_code == 200 or response.status_code == 201:
            logger.info("room_uuid: {} class created. {}".format(room_uuid_, response.json()))
            return response.json()
        else:
            logger.error("{} room_uuid: {}. Response: {}".format(function_name, room_uuid_, response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, room_uuid_), e)


def close_class(room_uuid_, class_config, token_admin):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/room/configuration/b0b999da-83e2-41a6-845d-491f0bb72698/state".format(room_uuid_)

        payload = "{\n    \"state\": \"reset\"\n}"
        headers = {
            'token-level': 'branch_tcpzqv10xdj25r67afsowuhk98n3e',
            'token-admin': '{{token_admin}}',
            'Accept': 'application/json',
            'Accept-Language': 'en',
            'Authorization': 'Bearer CvzRHIllLDD99FK4xoxfbBJTr0YcTkAOcGupPXIf',
            'Content-Type': 'application/json'
        }

        headers_ = headers.copy()
        headers_["token-admin"] = token_admin
        payload = class_config
        payload = json.dumps(payload)
        response = requests.post(url, headers=headers_, data=payload, timeout=10)
        if response.status_code == 200 or response.status_code == 201:
            logger.info("room_uuid: {} class created. {}".format(room_uuid_, response.json()))
            return response.json()
        else:
            logger.error("{} room_uuid: {}. Response: {}".format(function_name, room_uuid_, response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, room_uuid_), e)


def send_measurement(user_info_, class_uuid_, measurement_=None):
    function_name = get_this_method_name()
    try:
        url = __env__.api_realtime_hostname + "api/v2/measurements/class/{}".format(class_uuid_)
        headers_ = headers.copy()
        payload = {}
        payload["user_uuid"] = user_info_["user_uuid"]
        payload["pseudonym"] = user_info_["pseudonym"]
        payload["email"] = user_info_["email"]

        payload["steps_tot"] = measurement_["steps_tot"]
        payload["calories_tot"] = measurement_["calories_tot"]
        payload["hr"] = measurement_["hr"]
        payload["effort"] = measurement_["effort"]

        payload = json.dumps(payload)
        response = requests.post(url, headers=headers_, data=payload, timeout=10)
        if response.status_code == 200 or response.status_code == 201:
            # logger.info("Sent measurement. code[{}] of user: {}. {}".format(response.status_code, user_info_["user_uuid"], measurement_))
            return response.json()
        elif response.status_code == 307:
            # logger.info("Sent measurement. code[{}] of user: {}. {}".format(response.status_code, user_info_["user_uuid"], measurement_,))
            pass
        else:
            logger.error("{} user_uuid: {}. Response: {}".format(function_name, user_info_["user_uuid"], response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, user_info_["user_uuid"]), e)

