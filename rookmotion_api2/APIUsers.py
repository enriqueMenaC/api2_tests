import datetime
import inspect
import json
import os
import sys
import time
import random
from typing import Dict

import __env__
import requests


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from rookmotion_utilities.DebugHelper import get_this_method_name
from rookmotion_utilities.LoggerHelper import Logger
from rookmotion_api2.APICommon import headers, response_is_ok, string_to_dictionary


logger = Logger(name=os.path.basename(__file__), level="debug")


def get_random_variables():
    data_ = {"weight": random.randint(60, 100),
             "height": round(random.uniform(150.0, 180.0), 1),
             "resting_heart_rate": random.randint(60, 70)}
    return data_


def add_random_user():
    function_name = get_this_method_name()
    email = None
    try:
        url = __env__.api_hostname + "api/v2/users"

        email = "user_testing_{}".format(datetime.datetime.now())
        email = email.replace("_", "")
        email = email.replace("-", "")
        email = email.replace(" ", "")
        email = email.replace(":", "")
        email = email.replace(".", "")[:27]
        email = email + "@rookmotion.com"

        payload = {}
        payload["email"] = email
        payload = json.dumps(payload)
        response = requests.post(url, headers=headers, data=payload, timeout=10)

        if response_is_ok(response):
            logger.info("Added user: {}".format(email))
            return email
        else:
            logger.error("Failed to add user: {}. {}".format(email, response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, email), e)


def get_user_uuid(email: str):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/users/{}/status".format(email)

        payload = {}
        response = requests.get(url, headers=headers, data=payload, timeout=10)
        if response_is_ok(response):
            logger.info("User information of user_email: {}. {}".format(email, response.json()))
            return response.json()
        else:
            logger.error("Failed to get user: {}".format(email))
    except Exception as e:
        logger.exception("{}: ".format(function_name, email), e)


def set_user_password(user_uuid_: str, password: str):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/auth/user/{}/password".format(user_uuid_)
        payload = {}
        payload["password"] = password
        payload["password_confirmation"] = password
        payload = json.dumps(payload)
        response = requests.post(url, headers=headers, data=payload, timeout=10)
        if response_is_ok(response):
            logger.info("Set password for user_uuid: {}".format(user_uuid_))
            return True
        else:
            logger.error("Failed to set password of user_uuid: {}. {}".format(user_uuid_, response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, user_uuid_), e)


def login_user(user_uuid_: str, password: str):
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/auth/user/{}/login".format(user_uuid_)
        headers_ = headers.copy()
        headers_["key-device"] = "12345678"
        headers_["product"] = "PC"
        headers_["model"] = "T470s"
        headers_["version"] = "1.0"

        payload = {}
        payload["password"] = password

        payload = json.dumps(payload)
        response = requests.post(url, headers=headers_, data=payload, timeout=10)
        if response_is_ok(response):
            logger.info("user_uuid: {} logged. {}".format(user_uuid_, response.json()))
            return response.json()
        else:
            logger.error("Failed to login user_uuid: {}. {}".format(user_uuid_, response.text))
    except Exception as e:
        logger.exception("{}: ".format(function_name, user_uuid_), e)


def add_physiological_variables(user_uuid_: str, variables_: Dict = None, token_user: str = None):
    function_name = get_this_method_name()
    try:
        headers_ = headers.copy()
        if token_user is None:
            headers_["token-level"] = __env__.token_client
        else:
            headers_["token-user"] = token_user

        url = __env__.api_hostname + "api/v2/users/{}/variables".format(user_uuid_)

        if variables_ is None:
            variables_ = get_random_variables()

        payload = json.dumps(variables_)
        response = requests.post(url, headers=headers_, data=payload, timeout=10)
        if response_is_ok(response):
            logger.info("Physiological variables added to user {}".format(user_uuid_))
            return True
        else:
            logger.error("Error adding physiological variables to user {}".format(user_uuid_))
            return False
    except Exception as e:
        logger.exception("{}: {}".format(function_name, user_uuid_), e)
    return False


def add_user_information(user_uuid_: str, information_: Dict, token_user: str = None):
    function_name = get_this_method_name()
    try:
        headers_ = headers.copy()
        if token_user is None:
            headers_["token-level"] = __env__.token_client
        else:
            headers_["token-user"] = token_user

        url = __env__.api_hostname + "api/v2/users/{}/information".format(user_uuid_)
        payload = json.dumps(information_)
        response = requests.put(url, headers=headers_, data=payload, timeout=10)
        if response_is_ok(response):
            logger.info("Info added to user {}".format(user_uuid_))
            return True
        else:
            logger.error("Error adding info to user {}".format(user_uuid_))
            return False
    except Exception as e:
        logger.exception("{}: {}".format(function_name, user_uuid_), e)
    return False


def create_random_user():
    user_email = add_random_user()
    user_info_ = get_user_uuid(user_email)
    user_info_["email"] = user_email
    user_uuid_ = user_info_["user_uuid"]

    password_ = "12345678"
    set_user_password(user_uuid_, password_)
    login_info = login_user(user_uuid_, password_)
    user_token = login_info["token_user"]
    user_info_["token_user"] = user_token

    timestamp = int(time.time())
    information = {"name": "test_{}".format(timestamp),
                   "last_name_1": "apellido_1_{}".format(timestamp),
                   "last_name_2": "apellido_2_{}".format(timestamp),
                   "pseudonym": "pseudonym_{}".format(timestamp),
                   "birthday": "1990-06-15",
                   "sex": "female" if timestamp % 2 else "male"}
    if add_user_information(user_uuid_, information, user_token):
        user_info_["information"] = information

    variables = get_random_variables()
    if add_physiological_variables(user_uuid_, variables, user_token):
        user_info_["physiological_variables"] = variables

    return user_info_


def get_user_information(user_uuid_: str, token_user: str = None):
    function_name = get_this_method_name()
    try:
        headers_ = headers.copy()
        if token_user is None:
            # With token_level -> branch : needs token_admin
            pass
        else:
            headers_["token-user"] = token_user

        url = __env__.api_hostname + "api/v2/users/{}/information".format(user_uuid_)
        response = requests.get(url, headers=headers_, timeout=10)
        if response_is_ok(response):
            information_ = string_to_dictionary(response)
            # logger.info("Information of user {}\n{}".format(user_uuid_, information_))
            logger.info("Information of user {} is ok".format(user_uuid_))
            return True
        else:
            logger.error("Error getting information of user {}".format(user_uuid_))
            return False
    except Exception as e:
        logger.exception("{}: {}".format(function_name, user_uuid_), e)


def get_center_users(from_date: str = None):
    # With token_level -> branch : needs token_admin
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/center/users"
        if from_date is not None:
            url += "?from_date={}".format(from_date)

        logger.info("Getting center users since {}".format(from_date))
        response = requests.get(url, headers=headers, timeout=10)
        if response_is_ok(response):
            response = string_to_dictionary(response)
            data = response["data"]
            return data
    except Exception as e:
        logger.exception("{}: ".format(function_name), e)


def get_users(from_date: str = None):
    # With token_level -> branch : needs token_admin
    function_name = get_this_method_name()
    try:
        url = __env__.api_hostname + "api/v2/center/users"  # TODO Change api_action
        if from_date is not None:
            url += "?from_date={}".format(from_date)

        logger.info("Getting users since {}".format(from_date))
        response = requests.get(url, headers=headers, timeout=10)
        if response_is_ok(response):
            response = string_to_dictionary(response)
            data = response["data"]
            return data
    except Exception as e:
        logger.exception("{}: ".format(function_name), e)


if __name__ == "__main__":
    logger.info("Testing APIUsers")
    user_info = create_random_user()

    print(user_info)

    user_uuid = user_info["user_uuid"]
    user_token = user_info["token_user"]
    get_user_information(user_uuid, user_token)
    get_users()
    get_center_users()
