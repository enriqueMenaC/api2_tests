import datetime
import threading
import time

from faker import Faker

import FakeUsers
import RemoteRooms
import __env__

from ClassConfig import class_config
from rookmotion_api2.APIAdmin import login_admin
from rookmotion_api2.APIRemote import create_class, request_room_access, send_measurement
from rookmotion_utilities.LoggerHelper import Logger

fake = Faker()  # To generate fake data
logger = Logger(name='Test', level="debug")

if __env__.environment == "production":
    Question = input("Are you sure to run on production??")
    if Question == "Y":
        pass
    else:
        exit()


def measurements_thread(*args):
    target = args[0]
    thread = threading.Thread(target=target, args=args[1:])
    thread.start()


def loop_send_measurements(*args):
    user_info_ = args[0]
    class_uuid_ = args[1]
    room_ = args[2]
    coach_name = room_["room_coach"]
    pseudonym = (user_info_["email"].split("@")[0])
    if len(coach_name) + len(pseudonym) > 15:
        len_to_use = 14 - len(coach_name)
        pseudonym = coach_name + pseudonym[-len_to_use:]
    else:
        pseudonym = coach_name + pseudonym
    # pseudonym = fake.name().split(" ")[0]  # Uncomment to fake pseudonyms
    print("added {} ".format(pseudonym))
    user_info_["pseudonym"] = pseudonym

    for i in range(1, 3600):
        seconds = datetime.datetime.now().second
        if seconds == 0:
            seconds = 1
        measurement = {}
        measurement["steps_tot"] = i
        measurement["calories_tot"] = i
        measurement["hr"] = datetime.datetime.now().second
        measurement["effort"] = seconds * 2
        send_measurement(user_info_, class_uuid_, measurement)
        time.sleep(3)


if __name__ == "__main__":
    # branches_rooms = [RemoteRooms.roomsBranch1] # production
    branches_rooms = [RemoteRooms.roomsBranch3]  # develop
    # users_to_use = FakeUsers.UsersGroup1.copy()  # production
    users_to_use = FakeUsers.users_develop.copy()  # develop
    for branch_rooms in branches_rooms:
        token_level = branch_rooms[0]["token_level"]
        token_info = login_admin(token_level, "root@rookmotion.com", "#1PDlqJ2$3xX")
        token_admin_ = token_info["token_user"]
        # users_per_room = int(len(users_to_use) / len(RemoteRooms.roomsBranch1))
        users_per_room = 23
        logger.info("This will run for {} users per room".format(users_per_room))

        for room_i, room in enumerate(branch_rooms):
            logger.debug("Creating class for room: {} using token_admin: {}".format(room, token_admin_))
            class_info = create_class(room["room_uuid"], class_config, token_admin_, token_level)
            class_uuid = class_info["uuid"]
            logger.info(class_uuid)

            for user_i in range(users_per_room):
                user_to_use = users_to_use.pop()
                request_room_access(user_to_use["user_uuid"], user_to_use["token_user"], class_uuid)
                measurements_thread(loop_send_measurements, user_to_use, class_uuid, room)
